package sbp;

import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DuplicateSearchTest
{

 /**
  * @param myList - заполнение исходной коллекции.
  * @param resultList - заполнение коллекции дубликатов для сверки.
  */

    private List<String> myList = new ArrayList<>();
    private List<String> resultList = new ArrayList<>();

    @Before
    public void setTestList()
    {
        myList.add("y");
        myList.add("c");
        myList.add("x");
        myList.add("a");
        myList.add("r");
        myList.add("y");
        myList.add("r");
        myList.add("p");
        myList.add("p");
        myList.add("k");
        myList.add("x");

        resultList.add("y");
        resultList.add("x");
        resultList.add("r");
        resultList.add("p");
    }

    /**
     * Проверка коллекции на соответвие коллекции дубликатов
     * методом перебора (2 метода)
     */
    @Test
    public void getDuplicateOneTest()
    {
        assertTrue(resultList.containsAll(DuplicateSearch.duplicateSearch(myList)));
        assertTrue(resultList.containsAll(DuplicateSearch.duplicateSearchSet(myList)));
    }

    /**
     * Проверка коллекции на соответвие коллекции дубликатов
     * с использованием Map
     */
    @Test
    public void getDuplicateTwoTest()
    {
        assertTrue(resultList.containsAll(DuplicateSearch.duplicateSearchWithoutMap(myList)));
    }

    /**
     * Проверка коллекции на соответвие коллекции дубликатов
     * с использованием stream:
     * 1 - используя Set
     * 2 - используя Map
     * 3 - группировкой
     * 4 - частотой
     */
    @Test
    public void getDuplicateSearchWithoutStream()
    {
        assertTrue(resultList.containsAll(DuplicateSearch.duplicateSearchWithoutStreamFromSet(myList)));
        assertTrue(resultList.containsAll(DuplicateSearch.duplicateSearchWithoutStreamFromMap(myList)));
        assertTrue(resultList.containsAll(DuplicateSearch.duplicateSearchWithoutStreamFromGrouping(myList)));
        assertTrue(resultList.containsAll(DuplicateSearch.duplicateSearchWithoutStreamFromFrequency(myList)));
    }


    @Test
    public void testCheckBrackets()
    {
        String s = "(){}[]";
        String s1 = "[(])";
        String s2 = "({}[])";
        String s3 = ")";
        String s4 = "({[({((()))})]})";
        String s5 = "{([({)])}";

        DuplicateSearch call = new DuplicateSearch();

        boolean result = call.checkBrackets(s);
        boolean result1 = call.checkBrackets(s1);
        boolean result2 = call.checkBrackets(s2);
        boolean result3 = call.checkBrackets(s3);
        boolean result4 = call.checkBrackets(s4);
        boolean result5 = call.checkBrackets(s5);

        assertTrue(result);
        assertFalse(result1);
        assertTrue(result2);
        assertFalse(result3);
        assertTrue(result4);
        assertFalse(result5);
    }


}
