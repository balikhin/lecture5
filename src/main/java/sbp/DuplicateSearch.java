package sbp;
import java.util.*;
import java.util.stream.Collectors;
public class DuplicateSearch
{

    /**
     * Поиск дубликатов методом переборов.
     * @param collection - коллекция поиска
     * @return - коллекция дубликатов
     */
    public static <T> Collection<T> duplicateSearch(Collection<T> collection)
    {
        if (collection.isEmpty()) return new ArrayList<>();

        Set<T> result = new HashSet<>();
        for (T el : collection)
        {
            int count = 0;
            for (T elm : collection)
            {
                if (el.equals(elm))
                {
                    count++;
                }
            }
            if (count >= 2) result.add(el);
        }
        return result;
    }

    /**
     * Поиск дубликатов методом одноразового перебора с использованием Set.
     * @param collection - коллекция поиска
     * @return - коллекция дубликатов
     */
    public static <T> Collection<T> duplicateSearchSet(Collection<T> collection)
    {
        if (collection.isEmpty()) return new ArrayList<>();

        Set<T> temp = new HashSet<>();
        Set<T> result = new HashSet<>();
        for (T el : collection)
        {
            if (!temp.add(el))
            {
                result.add(el);
            }
        }
        return result;
    }

    /**
     * Поиск дубликатов через Map с использованием подсчета количества элементов.
     *      * @param collection - коллекция поиска
     *      * @return - коллекция дубликатов
     */
    public static <T> Collection<T> duplicateSearchWithoutMap(Collection<T> collection)
    {
        if (collection.isEmpty()) return new ArrayList<>();

        Map<T, Integer> map = new HashMap<>();
        for (T el : collection)
        {
            if (!map.containsKey(el) )
            {
                map.put(el, 1);
            } else {
                int count = map.get(el);
                map.put(el, ++count);
            }
        }

        List<T> result = new ArrayList<>();
        for (Map.Entry<T, Integer> el : map.entrySet()) {
            if (el.getValue() >= 2) result.add(el.getKey());
        }
        return result;
    }

    /**
     * Потск дубликатов с использованием stream через Set.
     *      * @param collection - коллекция поиска
     *      * @return - коллекция дубликатов
     */
    public static <T> Collection<T> duplicateSearchWithoutStreamFromSet(Collection<T> collection)
    {
        if (collection.isEmpty()) return new ArrayList<>();

        Set<T> result = new HashSet<>();

        return collection.stream()
                .filter(n -> !result.add(n))
                .collect(Collectors.toSet());
    }

    /**
     * Потск дубликатов с использованием stream через Map.
     *      * @param collection - коллекция поиска
     *      * @return - коллекция дубликатов
     */
    public static <T> Collection<T> duplicateSearchWithoutStreamFromMap(Collection<T> collection)
    {
        if (collection.isEmpty()) return new ArrayList<>();

        return collection.stream()
                .collect(Collectors.toMap(x -> x, x -> 1, Integer::sum))
                .entrySet()
                .stream()
                .filter(el -> el.getValue() >= 2)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    /**
     * Потск дубликатов с использованием stream через groupingBy.
     *      * @param collection - коллекция поиска
     *      * @return - коллекция дубликатов
     */
    public static <T> Collection<T> duplicateSearchWithoutStreamFromGrouping(Collection<T> collection)
    {
        if (collection.isEmpty()) return new ArrayList<>();

        return collection.stream()
                .collect(Collectors.groupingBy(x -> x, Collectors.counting()))
                .entrySet()
                .stream()
                .filter(el -> el.getValue() >= 2)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    /**
     * Потск дубликатов с использованием stream через frequency.
     *      * @param collection - коллекция поиска
     *      * @return - коллекция дубликатов
     */
    public static <T> Collection<T> duplicateSearchWithoutStreamFromFrequency(Collection<T> collection)
    {
        if (collection.isEmpty()) return new ArrayList<>();

        return collection.stream()
                .filter(i -> Collections.frequency(collection, i) > 1)
                .collect(Collectors.toSet());
    }



    /**
     * Принимает строку состоящую из скобочек, проверяет на правильность закрытия
     * @param s строка
     * @return true , если правильно закрыты. false, елси не правильно.
     */
    public boolean checkBrackets(String s)
    {
        Stack<Character> stack = new Stack();
        char[] array = s.toCharArray();

        int count = 0;
        for (int i = 0; i < array.length; i++) {

            if ((array[i] == '(') || (array[i] == '[') || (array[i] == '{'))
            {
                stack.push(array[i]);
                count++;

            }
            if ((array[i] == ')') || (array[i] == ']') || (array[i] == '}'))
            {
                if (!stack.isEmpty())
                {
                    if ((array[i]) == (')') && (char) stack.pop() == ('('))
                    {
                        count--;
                    }
                    if ((array[i]) == (']') && (char) stack.pop() == ('['))
                    {
                        count--;
                    }
                    if ((array[i]) == ('}') && (char) stack.pop() == ('{'))
                    {
                        count--;
                    }
                } else
                {
                    count++;
                }

            }

        }
        return count == 0;
    }

}